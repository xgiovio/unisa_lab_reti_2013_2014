#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fcntl.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <signal.h>

void sigusr1action (int signal){
     
     fprintf(stdout,"Parent:Ricevuto SIGUSR1\n");
     
}

void sigchldaction (int signal){

    fprintf(stdout,"Parent:Ricevuto SIGCHLD\n");

     
}


int main (int index, char** in_data){
     
    int duplicate;
    duplicate = fork();
    if (duplicate < 0)
          exit(-1);
    if (duplicate == 0){
     //child
     
          int numero;
          fscanf(stdin,"%d",&numero);
          kill (getppid(),SIGUSR1);
          fprintf(stdout,"Child:Process %d terminated\n",getpid());
     
    }else{
     // parent
     
          struct sigaction sigusr1structure;
               sigusr1structure.sa_handler = sigusr1action;
               sigusr1structure.sa_flags = 0;
          
         
          struct sigaction sigchldstructure;
               sigchldstructure.sa_handler = sigchldaction;
               sigchldstructure.sa_flags = 0;
          
         
         sigaction (SIGUSR1, &sigusr1structure, NULL);
         sigaction (SIGCHLD, &sigchldstructure, NULL);
         
          int status;
          int pid;
          
          for (pid = waitpid(-1,&status,0);pid > 0;pid = waitpid(-1,&status,0)){
               fprintf(stdout,"Parent:Child %d terminated\n",pid);
          }
         
         fprintf(stdout,"Parent:Process %d terminated\n",getpid());
    }
    
    
    
     
     return 0;
}

