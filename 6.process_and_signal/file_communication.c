#include <unistd.h>
#include <stdio.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fcntl.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>


void sigusr1action (int signal){
     
     fprintf(stdout,"Parent:Ricevuto SIGUSR1\n");
     
}

void sigusr1caction (int signal){
     
     fprintf(stdout,"Child:Ricevuto SIGUSR1\n");
     int fd;
     fd = open ("share_file", O_RDWR | O_APPEND);
     if (fd < 0){
          fprintf(stderr,"Child: Error reading file - Errno%d\n",errno);
          kill(getpid(),SIGKILL);
     }
     char a [100];
     char b [100];
     char op;
     lseek(fd,0,SEEK_SET);
     char data[2];
     data[1]=0;
     char input[100]="";
     for (read(fd,data,1);data[0]!='\n';read(fd,data,1)){
          strcat(input,data);
     }
     sscanf(input,"%s%*c%s%*c%c",a,b,&op);
     
     int a_data, b_data,calcolo;
     char calcolo_s[100];
     
     a_data= atoi(a);
     b_data = atoi(b);
     
     if (op=='+'){
          calcolo = a_data + b_data;
     }else{
               if (op=='-'){
                    calcolo = a_data - b_data;
               }else{
                         if (op=='*'){
                              calcolo = a_data * b_data;
                         }else{

                              calcolo = a_data / b_data;

                         }
               }
     }
     sprintf(calcolo_s,"%d",calcolo);
     write (fd,calcolo_s,strlen(calcolo_s));
     
     kill(getppid(),SIGUSR1); 
}




int main (int index, char** in_data){
     
     if (index < 4 || index > 4) {
          fprintf(stderr,"Parametri command line non validi\n");
          exit(-1);
     }
     
    int duplicate;
    duplicate = fork();
    if (duplicate < 0)
          exit(-1);
    if (duplicate == 0){
     //child
     
          
               
          struct sigaction sigusr1cstructure;
               sigusr1cstructure.sa_handler = sigusr1caction;
               sigusr1cstructure.sa_flags = 0;
          
          
         
         sigaction (SIGUSR1, &sigusr1cstructure, NULL);
         pause();
          
          fprintf(stdout,"Child:Process %d terminated\n",getpid());
     
    }else{
     // parent
     
     int fd;
     fd = open ("share_file", O_CREAT | O_WRONLY | O_EXCL,00700);
   if (fd < 0){
     fprintf(stderr,"Parent:Share_file already exists\n");
     fd = open ("share_file", O_WRONLY | O_TRUNC);
     if (fd < 0){
          fprintf(stderr,"Parent:Error writing to file - Err no%d\n",errno);
          char cmd [100] = "kill -P ";
          char pid [10];
          sprintf (pid,"%d",getpid());
          strcat(cmd,pid);
          system(cmd);
          
     }
   }
   
   write (fd,*(in_data+1),strlen(*(in_data+1)));
   write (fd," ",1);
   write (fd,*(in_data+2),strlen(*(in_data+2)));
   write (fd," ",1);
   write (fd,*(in_data+3),strlen(*(in_data+3)));
   write (fd,"\n",1);
   
   close(fd);
   
     
          struct sigaction sigusr1structure;
               sigusr1structure.sa_handler = sigusr1action;
               sigusr1structure.sa_flags = 0;
          
          
         
         sigaction (SIGUSR1, &sigusr1structure, NULL);
         
          int status;
          int pid;
          
     kill (duplicate,SIGUSR1);
          
     for (pid = waitpid(-1,&status,0);pid > 0;pid = waitpid(-1,&status,0)){
          fprintf(stdout,"Parent:Child %d terminated\n",pid);
     }
    
    fprintf(stdout,"Parent:Process %d terminated\n",getpid());
    
    }
    

     
     return 0;
}

