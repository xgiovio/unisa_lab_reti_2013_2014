
#include	"../basic.h"

int main(int argc, char **argv) {
	int			sockfd;
	struct sockaddr_in	servaddr;

	if (argc != 3){
		printf("usage: daytimecli <indirizzoIP> <porta> \n");
		exit(0);
}
        if( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0 ){
			printf("socket error\n");
			exit(0);
}
	servaddr.sin_family = AF_INET;
        servaddr.sin_port = htons(atoi(argv[2])); /* server port */ 

        if (inet_pton(AF_INET, argv[1], &servaddr.sin_addr) <= 0){
                printf("inet_pton error for %s\n", argv[1]);
		exit(0);
}
	if (connect(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0){
		printf("connect error\n");
		exit(0);
	}
	exit(0);
}
