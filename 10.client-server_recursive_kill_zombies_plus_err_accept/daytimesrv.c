
#include	"../basic.h"
#include	<time.h>



void sigchldaction (int signal){

	int status, pid;
     
   for (pid = waitpid(-1,&status,WNOHANG);pid > 0;pid = waitpid(-1,&status,WNOHANG)){
          fprintf(stdout,"Server:Child %d terminated\n",pid);
     }  


}

int main(int argc, char **argv) {
	
	
	int			listenfd, connfd, n;
	struct sockaddr_in	servaddr;
	char			buff[INET_ADDRSTRLEN];
	short port;

	if (argc != 2 ){
		printf("usage: daytimesrv <porta>\n");
		exit(0);
}
	if( (listenfd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
		printf("socket error\n");
		exit(0);
}
	servaddr.sin_family      = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);   /* wildcard address */
	servaddr.sin_port        = htons(atoi(argv[1])); /* server port */

	if( (bind(listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr))) < 0){
		printf("bind error\n"); exit(0);
}
	if( listen(listenfd, 5) < 0 )
		{ printf("listen error\n");
		  exit(0);
		}

	struct sockaddr_in	client_addr;
	unsigned int client_addr_len = sizeof(client_addr);

	struct sigaction sigchldstructure;
               sigchldstructure.sa_handler = sigchldaction;
               sigchldstructure.sa_flags = 0;
          
    sigaction (SIGCHLD, &sigchldstructure, NULL);

	for ( ; ; ) {
		if( (connfd = accept(listenfd, (struct sockaddr *) &client_addr, &client_addr_len)) < 0)
		{ 
			if(errno == EINTR || errno==ECONNABORTED)
				continue;
			printf("accept error\n");
			exit(1);

		}
		
		int split = fork();
		
		if ( split < 0) {
			printf("fork for iterative server failed\n");
			exit(-1);
		}
		
		if (split == 0){
			close(listenfd);
			
			inet_ntop(AF_INET, &(client_addr.sin_addr), buff, INET_ADDRSTRLEN); // get client address
			port = ntohs(client_addr.sin_port);// get client port
			fprintf (stdout,"%s%c%hu\n",buff,':',port);
			exit(0);
			
		} else {
			
			printf("connection managed by a child process\n");
			close(connfd);
		}
		
		

	}
}
