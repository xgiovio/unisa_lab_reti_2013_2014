#include <stdio.h>
#include <stdlib.h>


int main (int index, char** in_data){
     
     short data = 0x0102;
     char* datacp = (char*)&data;
     if (*datacp == 0x01 && *(datacp+1)==0x02){
          printf("Big Endian\n");
     }else{
          if(*datacp == 0x02 && *(datacp+1)==0x01){
               printf("Little Endian\n");    
          }else{
               printf("Unknow\n");
          }
     }
     
     
     return 0;
}