
#include	"../basic.h"
#include	<time.h>

int main(int argc, char **argv) {
	int			listenfd, connfd, n;
	struct sockaddr_in	servaddr;
	char			buff[INET_ADDRSTRLEN];

	if (argc != 2 ){
		printf("usage: daytimesrv <porta>\n");
		exit(0);
}
	if( (listenfd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
		printf("socket error\n");
		exit(0);
}
	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family      = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);   /* wildcard address */
	servaddr.sin_port        = htons(atoi(argv[1])); /* server port */

	if( (bind(listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr))) < 0){
		printf("bind error\n"); exit(0);
}
	if( listen(listenfd, 5) < 0 )
		{ printf("listen error\n");
		  exit(0);
		}

	struct sockaddr_in	client_addr;
	int client_addr_len = sizeof(client_addr);

	for ( ; ; ) {
		if( (connfd = accept(listenfd, (struct sockaddr *) &client_addr, &client_addr_len)) < 0)
		{ printf("accept error\n");
		exit(0);
		} 

		inet_ntop(AF_INET, &(client_addr.sin_addr), buff, INET_ADDRSTRLEN);
		fprintf (stdout,"%s\n",buff);
		close(connfd);
	}
}
