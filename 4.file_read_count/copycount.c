#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>


int main (int index, char** in_data){
     
     
   int fd,fd2,ridden,wrote, tot,vocali,cifre,righe;
   tot = vocali = cifre = righe = 0;
   char buf;
   fd = open ("sample",O_RDONLY);
   fd2 = open ("sample_copied", O_CREAT | O_WRONLY,00700);
   if (fd < 0){
     fprintf(stderr,"Error reading file %d\n",errno);
     exit(-1);
   }
   if (fd2 < 0){
     fprintf(stderr,"Error creating file %d\n",errno);
     exit(-1);
   }

     for (ridden = read (fd,&buf,1) ; ridden == 1 ;ridden = read (fd,&buf,1)){
          
          
          if (isdigit(buf) != 0){
               cifre++; tot++;
               wrote = write (fd2,&buf,1);
               if (wrote != 1){
                    fprintf(stderr,"Error writing to file %d\n",errno);
                    exit(-1);
               }
               continue;
          }
          
          if (buf == 'a' || buf == 'e' || buf == 'i' || buf == 'o' || buf == 'u'){
               vocali++;tot++;
               wrote = write (fd2,&buf,1);
               if (wrote != 1){
                    fprintf(stderr,"Error writing to file %d\n",errno);
                    exit(-1);
               }
               continue;
          }
          
          if (buf == '\n'){
               righe++;tot++;
               wrote = write (fd2,&buf,1);
               if (wrote != 1){
                    fprintf(stderr,"Error writing to file %d\n",errno);
                    exit(-1);
               }
               continue;
          }
          
          
          tot++;
          wrote = write (fd2,&buf,1);
          if (wrote != 1){
               fprintf(stderr,"Error writing to file %d\n",errno);
               exit(-1);
          }
 
          
     }
     
     if (ridden < 0){
          fprintf(stderr,"Error reading file %d\n",errno);
          exit(-1);
     }
     
     fprintf (stdout, "Numero vocali %d\n", vocali);
     fprintf (stdout, "Numero righe %d\n", righe);
     fprintf (stdout, "Numero cifre %d\n", cifre);
     fprintf (stdout, "Numero caratteri %d\n", tot);
     
     close(fd);close(fd2);
     
     
     return 0;
}