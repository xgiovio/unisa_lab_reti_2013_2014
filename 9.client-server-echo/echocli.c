/* Client che interroga un echo server */

#include	"basic.h"

void client_echo(FILE *fp, int sockfd); 

int main(int argc, char **argv) {
	int			sockfd, n;
	struct sockaddr_in	servaddr;

	if (argc != 3)
			{ printf("usage: echocli <IPaddress> <PORT>\n"); exit(1); }

	if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		{ printf("socket error\n"); exit(1); }

	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port   = htons(atoi(argv[2]));	/* echo server port */
	if (inet_pton(AF_INET, argv[1], &servaddr.sin_addr) <= 0)
		{ printf("inet_pton error for %s", argv[1]); exit(1);}

	if (connect(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0)
		{ printf("connect error\n"); exit(1);}

        client_echo(stdin, sockfd);	/* svolge tutto il lavoro del client */
	exit(0);
}


void client_echo(FILE *fp, int sockfd) {
        char    sendline[MAXLINE], recvline[MAXLINE];
        while (fgets(sendline, MAXLINE, fp) != NULL) {
                reti_writen(sockfd, sendline, strlen(sendline));
                if (reti_readline(sockfd, recvline, MAXLINE) == 0)
                        { printf("%s: server terminated prematurely",__FILE__); exit(1); }
                fputs(recvline, stdout);
        }
}

