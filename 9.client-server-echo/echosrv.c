/* Implementa un echo server che risponde alla porta definita dall'utente */
/* Crea un figlio per ogni client */

#include	"../basic.h"

void server_echo(int sockfd);

int main(int argc, char **argv) {
        pid_t			childpid;
	int			listenfd, connfd;
	struct sockaddr_in	servaddr, cliaddr;
	socklen_t		cliaddr_len;

        if( argc != 2){
           printf("Usage: echosrv <PORT> \n"); exit(1); 
}

	if( (listenfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		{ printf("socket error\n"); exit(1); }

	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family      = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);  /* wildcard address */
	servaddr.sin_port        = htons(atoi(argv[1]));	/* echo server */

	if( (bind(listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr))) < 0)
		 { printf("bind error\n"); exit(1); }

	if( listen(listenfd, BACKLOG) < 0 )
		{ printf("listen error\n"); exit(1);}

	for ( ; ; ) {
		cliaddr_len = sizeof(cliaddr);
		if( (connfd = accept(listenfd, (struct sockaddr *) &cliaddr, &cliaddr_len)) < 0)
			{ printf("accept error\n"); exit(1); }

                if( (childpid = fork()) == 0 ) {
		    close(listenfd);
		    server_echo(connfd);      /* svolge tutto il lavoro del server */
		    exit(0);
		}

		close(connfd);
	}
}

void server_echo(int sockfd) {
        ssize_t         n;
        char            line[MAXLINE];
        for ( ; ; ) {
                if ( (n = reti_readline(sockfd, line, MAXLINE)) == 0)
		{
			printf("\n Connessione chiusa dal client \n");
                        return;         /* connection closed by other end */
		}
	      reti_writen(sockfd, line, n);
        }
}

