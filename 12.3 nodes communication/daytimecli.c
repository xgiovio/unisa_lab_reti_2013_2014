

#include	"../basic.h"
#include    <unistd.h>

int main(int argc, char **argv) {
	int			sockfd;
	struct sockaddr_in	servaddr;

	if (argc != 3){
		printf("usage: daytimecli <indirizzoIP> <porta> \n");
		exit(0);
}
        if( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0 ){
			printf("socket error\n");
			exit(0);
}
	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
        servaddr.sin_port = htons(atoi(argv[2])); /* server port */ 

        if (inet_pton(AF_INET, argv[1], &servaddr.sin_addr) <= 0){
                printf("inet_pton error for %s\n", argv[1]);
		exit(0);
}
	if (connect(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0){
		printf("connect error\n");
		exit(0);
	}
	
	struct sockaddr_in	r_server_local_addr;
	unsigned int r_server_local_addr_len = sizeof(r_server_local_addr);
	
	char * socket_buffer = (char *) malloc (r_server_local_addr_len) ;
	int ridden;
	ridden = read (sockfd, socket_buffer,r_server_local_addr_len );
	fprintf(stdout,"Ho letto la prima volta %d bytes\n",ridden);
	ridden = read (sockfd, socket_buffer,r_server_local_addr_len );
	fprintf(stdout,"Ho letto la seconda volta %d bytes\n",ridden);


	fprintf(stdout,"I'm sleeping for 20sec\n");
	int  left = sleep(20);
	if (left == 0){
		  fprintf(stdout,"I'm here after 20 sec\n'");
		}else{
		  fprintf(stdout,"Waked up before 20 sec\n'");
		}


	ridden = read (sockfd, socket_buffer,r_server_local_addr_len );
	if (ridden < 0){
		fprintf(stdout,"Errore durante la lettura\n");
		exit(-1);
	} else {
		fprintf(stdout,"Ho letto la terza volta %d bytes\n",ridden);
	}




	
	memcpy (&r_server_local_addr,socket_buffer, r_server_local_addr_len);
	
	
	char buff[INET_ADDRSTRLEN];
	short port;
	
	inet_ntop(AF_INET, &(r_server_local_addr.sin_addr), buff, INET_ADDRSTRLEN);// get ip
	port = ntohs(r_server_local_addr.sin_port);// get  port
		
	fprintf (stdout,"%s%c%hu\n",buff,':',port);
	
	
	exit(0);
}
