

#include	"../basic.h"
#include	<time.h>

int main(int argc, char **argv) {
	int			listenfd, connfd, n;
	struct sockaddr_in	servaddr;
	char			buff[INET_ADDRSTRLEN];
	short			port;

	if (argc != 2 ){
		printf("usage: daytimesrv <porta>\n");
		exit(0);
}
	if( (listenfd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
		printf("socket error\n");
		exit(0);
}
	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family      = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);   /* wildcard address */
	servaddr.sin_port        = htons(atoi(argv[1])); /* server port */

	if( (bind(listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr))) < 0){
		printf("bind error\n"); exit(0);
}
	if( listen(listenfd, 5) < 0 )
		{ printf("listen error\n");
		  exit(0);
		}

	struct sockaddr_in	client_addr;
	unsigned int client_addr_len = sizeof(client_addr);
	
	struct sockaddr_in	server_local_addr;
	unsigned int server_local_addr_len = sizeof(server_local_addr);
	
	char * socket_buffer = (char *) malloc (server_local_addr_len) ;
	
	

	for ( ; ; ) {
		if( (connfd = accept(listenfd, (struct sockaddr *) &client_addr, &client_addr_len)) < 0)
		{ printf("accept error\n");
		exit(0);
		} 

		inet_ntop(AF_INET, &(client_addr.sin_addr), buff, INET_ADDRSTRLEN);// get client ip
		port = ntohs(client_addr.sin_port);// get client port
		
		fprintf (stdout,"%s%c%hu\n",buff,':',port);
		
		
		
		getsockname (connfd, (struct sockaddr *) &server_local_addr, &server_local_addr_len);
		memcpy (socket_buffer, &server_local_addr, server_local_addr_len);
		write (connfd,socket_buffer, server_local_addr_len);
		
		
		close(connfd);
	}
}
