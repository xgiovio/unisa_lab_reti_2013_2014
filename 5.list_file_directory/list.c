#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fcntl.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>


int main (int index, char** in_data){
     
     if (index < 2){
          fprintf(stderr,"Please specify a directory into command line\n");
          exit(-1);
     }
     
     if (index > 2){
          fprintf(stderr,"Please enter only 1 path\n");
          exit(-1);
     }
     
     
     char * path;
     path = *(in_data + 1);
     
     DIR * directory;
     directory =  opendir (path);
     if ( directory == NULL){ fprintf(stderr, "Not a directory\n"); exit(-1);}
     
     
     struct dirent * dir_entry;
     struct stat file_stat;
     char full_path [2048];
     
     int fd,ridden;
     char buf[10];
     
     for(dir_entry = readdir(directory); 
            dir_entry != NULL;              
            dir_entry = readdir(directory)){
         
         strcpy (full_path,path);
         strcat (full_path,"/");
         strcat (full_path,dir_entry->d_name);
         
         if (stat (full_path,&file_stat) != 0 ) {fprintf(stderr, "Error retrieving file info\n"); exit(-1);}
         
         fprintf (stdout,"Name %20s",dir_entry->d_name);
         fprintf (stdout," / ");
         fprintf (stdout,"Inode %10d",file_stat.st_ino);
         fprintf (stdout," / ");
         fprintf (stdout,"Size %10d",file_stat.st_size);
         fprintf (stdout," / ");
         
         if (S_ISREG (file_stat.st_mode)){
               fprintf (stdout,"%23s","Regular File / ");
               fd = open (full_path,O_RDONLY);
               if (fd < 0){
                fprintf(stderr,"Error reading file %s %d\n",dir_entry->d_name,errno);
                exit(-1);
               }
               ridden = read (fd,buf,10);
               buf[ridden] = 0;
               fprintf (stdout,"%10s",buf);
               
               close (fd);
         } else {
               fprintf (stdout,"%23s","Not a Regular File / ");
          
         }
         fprintf (stdout,"\n");
            
     }
        
     if (errno != 0){
     fprintf(stderr, "Error reading in the directory\n"); exit(-1);
     }
     
     
     
     return 0;
}