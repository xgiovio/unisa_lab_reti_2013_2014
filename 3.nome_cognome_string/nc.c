#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>


int main (int index, char** in_data){
     
     char buf[100];
     char bufn1[100];
     char bufn2[100];
     int num1,num2,sum;
     
     
     fprintf (stdout,"Digitare 2 numeri separati da uno spazio\n");
     fscanf (stdin, "%99[^\n]%*c",buf);
     sscanf (buf,"%s %s", bufn1, bufn2);
     num1 = atoi(bufn1);
     num2 = atoi(bufn2);
     sum = num1 + num2;
     
     fprintf(stdout,"La somma e' %d\n", sum);
     
     
     
     fprintf (stdout,"Digitare un nome\n");
     fscanf (stdin, "%99[^\n]%*c",bufn1);
     fprintf (stdout,"Digitare un cognome\n");
     fscanf (stdin, "%99[^\n]",bufn2);
     
     char bufsum[200];
     
     

     strcpy(bufsum, bufn1);
     strcat(bufsum, "-");
     strcat(bufsum, bufn2);
     
     fprintf(stdout,"%s\n",bufsum);
     
     
     strcpy(bufsum, bufn2);
     strcat(bufsum, "-");
     strcat(bufsum, bufn1);
     fprintf(stdout,"%s\n",bufsum);
     
     
     if (strcmp(bufn1,bufn2) == 0){
          fprintf(stdout,"Stringhe uguali\n");
     } else {
          fprintf(stdout,"Stringhe diverse\n");
     }
     
     
     char iniziali[3];
     iniziali[0] = bufn1[0];
     iniziali[1] = bufn2[0];
     iniziali[2] = 0;
     
     
     fprintf(stdout,"Iniziali %s\n",iniziali);
     
     char code[150];
     char sumc[150];
     sprintf (sumc,"%d",sum);
     
     strcpy(code, iniziali);
     strcat(code, sumc);
     
     fprintf(stdout,"Codice %s\n",code);
     
     
     
     
     return 0;
}