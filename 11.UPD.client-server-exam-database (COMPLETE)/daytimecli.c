#include    "../basic.h"
#include    <unistd.h>

void sigpipeaction (int signal){
    
    
    //fprintf(stderr, "Sigpipe catturato\n");
    
    
}


typedef struct message {
	char data [50];
	char nome [50];
	char voto [50];
	struct sockaddr_in addresses;
	unsigned int addresses_len;
}t_message;

int main(int argc, char **argv) {
    
    
    
    struct sigaction sigpipestructure;
    sigpipestructure.sa_handler = sigpipeaction;
    sigpipestructure.sa_flags = 0;
    
    sigaction (SIGPIPE, &sigpipestructure, NULL);
    
    
    
    
    
	int			sockfd;
	struct sockaddr_in	servaddr;

	if (argc != 3){
		printf("usage: daytimecli <indirizzoIP> <porta> \n");
		exit(0);
}
        if( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ){
			printf("socket error\n");
			exit(0);
}
	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
        servaddr.sin_port = htons(atoi(argv[2])); /* server port */ 

        if (inet_pton(AF_INET, argv[1], &servaddr.sin_addr) <= 0){
                printf("inet_pton error for %s\n", argv[1]);
		exit(0);
}
	if (connect(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0){
		printf("connect error\n");
		exit(0);
	}
	
	
	int ridden;
	int written;
	t_message m;
		m.addresses_len = sizeof (m.addresses);
		
	// get local address after connect	
	getsockname(sockfd, (struct sockaddr *) &(m.addresses) , &(m.addresses_len) );
	// getpeername per ottenere invece informazioni dall'altro lato di una connessione
	
	// send local address to the server
	for (written = write (sockfd, &m, sizeof (m)  ); written == -1; written = write (sockfd, &m, sizeof (m)) ){
		if (errno == EINTR){continue;} else {exit(0);}
	}

    

        //// welcome from server - first message
		for (ridden = read (sockfd,&m,sizeof(m)); ridden <= 0;ridden = read (sockfd,&m,sizeof(m))){
			if (errno == EINTR){continue;} else {exit(0);}
		}
	fprintf(stdout,"Server: %s\n",m.data);// answer from server

	char  line [1000];
	char  line_data [50];
	char  line_nome [50];
	char  line_voto [50];
	for(; ;){
		fprintf(stdout, "Inserisci comando: ");
		fscanf (stdin,"%999[^\n]%*c",line);
		sscanf (line, "%s", line_data);
		if (strcmp("AGGIUNGI",line_data) == 0 || strcmp("VERIFICA",line_data) == 0){
			
			if (strcmp("AGGIUNGI",line_data) == 0 ){
				sprintf(m.data,"AGGIUNGI");
				sprintf(line_nome,"def");
				sprintf(line_voto,"def");
				
				sscanf (line, "%*s %s %s", line_nome,line_voto);
				if (strcmp(line_nome,"def") == 0 || strcmp(line_voto,"def") == 0){
					fprintf(stdout,"Comando invalido\n");
					continue;
				}else{
					sprintf(m.nome,"%s",line_nome);
					sprintf(m.voto,"%s",line_voto);
				}
				
			} else {
				sprintf(m.data,"VERIFICA");
				sprintf(line_nome,"def");
				sscanf (line, "%*s %s ", line_nome);
				if (strcmp(line_nome,"def") == 0 ){
					fprintf(stdout,"Comando invalido\n");
					continue;
				}else{
					sprintf(m.nome,"%s",line_nome);
				}

			}
			
			for (written = write (sockfd, &m, sizeof (m)  ); written == -1; written = write (sockfd, &m, sizeof (m)) ){
				if (errno == EINTR){continue;} else {exit(0);}
			}
			for (ridden = read (sockfd,&m,sizeof(m)); ridden <= 0;ridden = read (sockfd,&m,sizeof(m))){
				if (errno == EINTR){continue;} else {exit(0);}
			}
			fprintf(stdout,"Server: %s\n",m.data);// answer from server
			
			
		} else {
			sprintf(m.data,"Bye");
			
			for (written = write (sockfd, &m, sizeof (m)  ); written == -1; written = write (sockfd, &m, sizeof (m)) ){
				if (errno == EINTR){continue;} else {exit(0);}
			}
			
			for (ridden = read (sockfd,&m,sizeof(m)); ridden <= 0;ridden = read (sockfd,&m,sizeof(m))){
				if (errno == EINTR){continue;} else {exit(0);}
			}	
			
			fprintf(stdout,"Server: %s\n",m.data);// answer from server
			break;
			
		}
	}
	

	
	exit(0);
}
