

#include	"../basic.h"
#include	<time.h>






void sigpipeaction (int signal){
    
    //fprintf(stderr, "Sigpipe catturato\n");
    
}




typedef struct message {
	char data [50];
	char nome [50];
	char voto [50];
	struct sockaddr_in addresses;
	unsigned int addresses_len;
}t_message;

int main(int argc, char **argv) {

//////////////////////////////////////////////////////////// SIGNALS

///////SIGPIPE
struct sigaction sigpipestructure;
    sigpipestructure.sa_handler = sigpipeaction;
    sigpipestructure.sa_flags = 0;
    
sigaction (SIGPIPE, &sigpipestructure, NULL);
//////////////////////////////////////////////////////////// END SIGNALS


	int			listenfd,n;
	struct sockaddr_in	servaddr;
	char			buff[INET_ADDRSTRLEN]; // client ip grabbed redeable
	short			port; // client port redeable

	if (argc != 2 ){
		printf("usage: daytimesrv <porta>\n");
		exit(0);
	}	
	if( (listenfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0){
		printf("socket error\n");
		exit(0);
	}
	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family      = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);   /* wildcard address */
	servaddr.sin_port        = htons(atoi(argv[1])); /* server port */

	if( (bind(listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr))) < 0){
		printf("bind error\n"); exit(0);
	}
	
	
	t_message m; // buffer message structure
	int ridden, written;
	
	
	for (;;){
	
	
	
		// read address from client
		for (ridden = read (listenfd,&m,sizeof(m)); ridden <= 0;ridden = read (listenfd,&m,sizeof(m))){
			if (errno == EINTR){continue;} else {exit(0);}
		}
	
	
		struct sockaddr_in	client_addr;
		unsigned int client_addr_len = sizeof(client_addr);
		
		client_addr = m.addresses;
		
		if (connect(listenfd, (struct sockaddr *) &client_addr, client_addr_len) < 0){
			printf("connect error\n");
			exit(0);
		}
		
		
	
	
		sprintf ( m.data, "Benvenuto" );
		//write with error check
		for (written = write (listenfd, &m, sizeof (m)  ); written == -1; written = write (listenfd, &m, sizeof (m)) ){
			if (errno == EINTR){continue;} else {exit(0);}
		}
	
		
		inet_ntop(AF_INET, &(client_addr.sin_addr), buff, INET_ADDRSTRLEN); // get client address
		port = ntohs(client_addr.sin_port);// get client port
		//fprintf (stdout,"%s%c%hu\n",buff,':',port);  // print client address and port
	
	
		for ( ; ; ) {// the server child reserve other requests from same client
			
			for (ridden = read (listenfd,&m,sizeof(m)); ridden <= 0;ridden = read (listenfd,&m,sizeof(m))){
				if (errno == EINTR){continue;} else {exit(0);}
			}
	
			int fd;	
	
			if (strcmp (m.data, "AGGIUNGI" ) == 0 ){
				   fd = open ("archivio.txt",O_RDWR | O_APPEND);
				   if (fd < 0){
						fd = open ("archivio.txt", O_CREAT | O_RDWR | O_APPEND ,00700);
				   }
				if (fd < 0){
					sprintf ( m.data, "Non posso modificare il database" );
						for (written = write (listenfd, &m, sizeof (m)  ); written == -1; written = write (listenfd, &m, sizeof (m)) ){
							if (errno == EINTR){continue;} else {exit(0);}
						}
				} else {
	
					// local writes. no checks
					write (fd,(m.nome), strlen ((m.nome)));
					write (fd," ", 1);
					write (fd,(m.voto), strlen ((m.voto)) );
					write (fd," ", 1);
					write (fd,buff, strlen (buff));
					write (fd,":", 1);
	
					char port_char[50];
					sprintf (port_char,"%hu",port);
					write (fd,port_char, strlen (port_char));
					write (fd,"\n", 1); 
	
	
					sprintf ( m.data, "AGGIUNTO" );
					for (written = write (listenfd, &m, sizeof (m)  ); written == -1; written = write (listenfd, &m, sizeof (m)) ){
						if (errno == EINTR){continue;} else {exit(0);}
					}
						
					close(fd);
				    }
			} else {
				if (strcmp (m.data, "VERIFICA" ) == 0 ){
	
					fd = open ("archivio.txt",O_RDONLY);
					if (fd < 0){
					sprintf ( m.data, "Non posso leggere il database" );
						for (written = write (listenfd, &m, sizeof (m)  ); written == -1; written = write (listenfd, &m, sizeof (m)) ){
							if (errno == EINTR){continue;} else {exit(0);}
						}
					} else {
	
						char  line [1000];
						char  line_nome [50];
						char  line_voto [50];
						char  line_ip_port [50];
						int returned,found;
	
						found = 0;
						FILE * fdfile = fdopen(fd,"r");
						returned = fscanf (fdfile,"%999[^\n]%*c",line);
						for( ; returned > 0 ;  returned = fscanf (fdfile,"%999[^\n]%*c",line) ){
	
							sscanf (line, "%s %s %s", line_nome, line_voto, line_ip_port);
	
							if (strcmp (line_nome, m.nome) == 0){
								found = 1;
	
								sprintf ( m.data, "%s ha sostenuto l'esame con voto %s. Informazione fornita da %s ", line_nome, line_voto, line_ip_port );
								for (written = write (listenfd, &m, sizeof (m)  ); written == -1; written = write (listenfd, &m, sizeof (m)) ){
									if (errno == EINTR){continue;} else {exit(0);}
								}
	
								break;
							} 
	
						}
	
						if (found == 0){
							sprintf ( m.data, "%s non ha sostenuto l'esame", m.nome );
							for (written = write (listenfd, &m, sizeof (m)  ); written == -1; written = write (listenfd, &m, sizeof (m)) ){
								if (errno == EINTR){continue;} else {exit(0);}
							}
						}
						close(fd);
					}
	
				} else {
	
				sprintf ( m.data, "Bye" );
						for (written = write (listenfd, &m, sizeof (m)  ); written == -1; written = write (listenfd, &m, sizeof (m)) ){
							if (errno == EINTR){continue;} else {exit(0);}
						}
				m.addresses.sin_family = AF_UNSPEC;
				connect(listenfd, (struct sockaddr *) &m.addresses, sizeof(m.addresses));
				break; // end requests from client
	
				}
			}
	
		}
	
	
	}
}
