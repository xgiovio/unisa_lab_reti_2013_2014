

#include	"../basic.h"

int main(int argc, char **argv) {
	int			sockfd;
	struct sockaddr_in	servaddr;

	if (argc != 3){
		printf("usage: daytimecli <indirizzoIP> <porta> \n");
		exit(0);
}
        if( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0 ){
			printf("socket error\n");
			exit(0);
}
	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
        servaddr.sin_port = htons(atoi(argv[2])); /* server port */ 

        if (inet_pton(AF_INET, argv[1], &servaddr.sin_addr) <= 0){
                printf("inet_pton error for %s\n", argv[1]);
		exit(0);
}
	if (connect(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0){
		printf("connect error\n");
		exit(0);
	}
	
	char server_address_redeable[INET_ADDRSTRLEN];
	short server_port_redeable;

	struct sockaddr_in	server_struct;
	unsigned int server_struct_size = sizeof(server_struct);
	

	int ridden;
	ridden = read (sockfd, &server_struct,server_struct_size );
	//fprintf(stdout,"Ho letto  %d\n bytes",ridden);
	
	
	inet_ntop(AF_INET, &(server_struct.sin_addr), server_address_redeable, INET_ADDRSTRLEN);// get ip
	server_port_redeable = ntohs(server_struct.sin_port);// get  port
		
	fprintf (stdout,"%s%c%hu\n",server_address_redeable,':',server_port_redeable);
	
	
	exit(0);
}
