#include    "../basic.h"
#include    <unistd.h>

void sigpipeaction (int signal){
    
	int status, pid;
    
    fprintf(stderr, "Sigpipe catturato\n");
    
    
}


int main(int argc, char **argv) {
    
    
    
    struct sigaction sigpipestructure;
    sigpipestructure.sa_handler = sigpipeaction;
    sigpipestructure.sa_flags = 0;
    
    sigaction (SIGPIPE, &sigpipestructure, NULL);
    
    
    
    siginterrupt (SIGPIPE,1);
    
    
    
    
	int			sockfd;
	struct sockaddr_in	servaddr;

	if (argc != 3){
		printf("usage: daytimecli <indirizzoIP> <porta> \n");
		exit(0);
}
        if( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0 ){
			printf("socket error\n");
			exit(0);
}
	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
        servaddr.sin_port = htons(atoi(argv[2])); /* server port */ 

        if (inet_pton(AF_INET, argv[1], &servaddr.sin_addr) <= 0){
                printf("inet_pton error for %s\n", argv[1]);
		exit(0);
}
	if (connect(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0){
		printf("connect error\n");
		exit(0);
	}
	
	struct sockaddr_in	r_server_local_addr;
	unsigned int r_server_local_addr_len = sizeof(r_server_local_addr);
	
	char * socket_buffer = (char *) malloc (r_server_local_addr_len) ;
	int ridden;
	int written;


	sleep(5);
    
    int i;
    for ( i = 0; i < 5; i ++){
        
	ridden = read (sockfd, socket_buffer,r_server_local_addr_len );
	if (ridden < 0){
		fprintf(stdout,"Errore durante la lettura\n");
		exit(-1);
	} else {
		fprintf(stdout,"Ho letto  %d bytes\n",ridden);
	}
    }
    
    for ( i = 0; i < 1; i ++){
        
	written = write (sockfd, socket_buffer,r_server_local_addr_len /2  );
	if (written < 0){
		fprintf(stdout,"Errore durante la scrittura\n");
		exit(-1);
	} else {
		fprintf(stdout,"Ho scritto  %d bytes\n",written);
	}
    }
    

  

	
	memcpy (&r_server_local_addr,socket_buffer, r_server_local_addr_len);
	
	
	char buff[INET_ADDRSTRLEN];
	short port;
	
	inet_ntop(AF_INET, &(r_server_local_addr.sin_addr), buff, INET_ADDRSTRLEN);// get ip
	port = ntohs(r_server_local_addr.sin_port);// get  port
		
	fprintf (stdout,"%s%c%hu\n",buff,':',port);
	
	
	
	exit(0);
}
