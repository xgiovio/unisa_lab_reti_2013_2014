Definizione 
Letteralmente significa presa (di corrente). Realizzano l'astrazione di canali
di comunicazione fra due computer connessi da una rete. Una coppia di socket 
consente la comunicazione.
Concatenazione di un indirizzo IP e di una porta. Un socket individua 
univocamente un processo in esecuzione su un dato host.

Indirizzo ip 
Stringa di 32 bit
Rappresentazione decimale

Numeri di porta
porta ben note da 0 a 1023
porta da 1024 a 49151 (possono essere registrate per servizi)
porte dinamiche da 49152 a 65535 (non registrabili, usabili liberamente).

Socket API
Collezione di strutture dati e di funzioni che permettono al programmatore di 
scrivere in modo semplice programmi in grado di comunicare sulla interrete.
Permette di: creare socket - leggere dati - scrivere dati sul canale

Comunicazione client-server
Client		Server
socket()	socket()
1.connect()	bind()
2.write()	listen()
3.read()	accept()
4.close()	1.
		2.read()
		3.write()
		4.read()
		close()

Abbiamo le seguenti fasi: 
In attesa di una connessione, fase di preparazione (Server)
Scambio di dati Client-Server

Rappresentazione di un Socket
struct in_addr
struct sockaddr_in {
 unit8_t	sin_len;
 sa_family_t	sin_family; //tipo di protocollo
 in_port_t	sin_port;  // 16-bit network byte ordered
 struct in_addr	sin addr; // struttura indirizzo IP
}
struct sockaddr {
 unit8_t	sin_len;
 sa_family_t	sin_family; 
 char		sa_data[14];
}

Funzioni principali
#include <sys/socket.h>

 int socket(int family,int type, int protocol) //Definisco il protocollo
 return -1 //Error
 return Socket descriptor (é come un file descriptor)
 
 [	int family
 	AF_INET  IPv4
 	AF_INET6 IPv6
 	AF_LOCAL
 	AF_ROUTE
 	altri...

 	type 
 	SOCK_STREAM
	
	 protocol
	 0 //lascia fare al sistema 
 ]

int connect(int sd, struct sockaddr *servaddr, socklen_t addrlen);

 -Permette di aprire una connessione client-server
 -Il kernel sceglie una porta e l'indirizzo IP
 -Viene effettuata la fase di handshake, in caso di errore ritorna
	-ETIMEDOUT	//Passa troppo tempo
 	-ECONNREFUSED	//Connessione rifiutata
	-EHOSTUNREACH	//Host non raggiungibile


int bind(int sd, struct sockaddr*myaddr, sockelen_t addrlen);

 -Permette ad un server di assegnare un indirizzo per il server al socket
 -Tipo indirizzo: IP - Porta - Entrambi - Nessuno (Sceglie sistema)
 -Default : 0

int listen(int sd, int backlog);

 -Permette di convertire il socket da attivo a passivo in modo che il kernel
  accetti connessioni sul socket
  Default socket attivo. 
 -Permette con backlog di specificare quante connessioni possono essere 
  attivate

int accept(int sd, struct sockaddr*cliaddr, socklen_t *addrlen);

 -Permette ad un server di prendere la prima connessione completata dalla coda
  Se non ce ne sono si blocca
 -addrlen è un parametro valore-risultato
  In chiamata contiene il valore della struttura passata 
  Al ritorno contiene la lunghezza della struttura piena

A questo punto la connessione è stabilita. Per terminare si utilizza close().  
