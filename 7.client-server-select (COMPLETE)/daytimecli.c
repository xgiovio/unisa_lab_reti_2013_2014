

#include	"../basic.h"

typedef struct message {
	char data[100];
}t_message;

void sigpipeaction (int signal){
    
    //fprintf(stderr, "Sigpipe catturato\n");
    
}

int main(int argc, char **argv) {
	
//////////////////////////////////////////////////////////// SIGNALS

///////SIGPIPE
struct sigaction sigpipestructure;
    sigpipestructure.sa_handler = sigpipeaction;
    sigpipestructure.sa_flags = 0;
    
sigaction (SIGPIPE, &sigpipestructure, NULL);
//////////////////////////////////////////////////////////// END SIGNALS

	int			sockfd;
	struct sockaddr_in	servaddr;

	if (argc != 3){
		printf("usage: daytimecli <indirizzoIP> <porta> \n");
		exit(0);
}
        if( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0 ){
			printf("socket error\n");
			exit(0);
}
	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
        servaddr.sin_port = htons(atoi(argv[2])); /* server port */ 

        if (inet_pton(AF_INET, argv[1], &servaddr.sin_addr) <= 0){
                printf("inet_pton error for %s\n", argv[1]);
		exit(0);
}
	if (connect(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0){
		printf("connect error\n");
		exit(0);
	}
	
	t_message m;
	fprintf (stdout,"Send Text: ");
	fscanf (stdin,"%999[^\n]%*c",m.data);
	int written;
	for (written = write (sockfd,&m,sizeof(m)); written <= 0;written = write (sockfd,&m,sizeof(m))){
		if (errno == EINTR){continue;} else {break;}
	}
	int ridden;
	if(written > 0){
		for (ridden = read (sockfd,&m,sizeof(m)); ridden <= 0;ridden = read (sockfd,&m,sizeof(m))){
			if (errno == EINTR){continue;} else {break;}
		}
		
		if(ridden > 0){
		
			fprintf(stdout,"%s from server\n",m.data);
		} else {
			
			fprintf(stdout,"can't read from server\n",m.data);
		}
		
		
	} else {
		fprintf(stdout,"can't write to server\n",m.data);
	}

	exit(0);
}
